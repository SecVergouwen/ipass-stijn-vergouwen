create table lid(
accountID int not null,
naam varchar(40),
wachtwoord varchar(20),
email varchar(30),
openingsrecht int,
primary key(accountID));

create table training(
trainingID int not null,
datum date,
openingstijd varchar(5),
openaar int,
primary key(trainingID));

create table isaanwezig(
accountID int,
trainingID int,
foreign key(accountID) references lid(accountID),
foreign key(trainingID) references training(trainingID));