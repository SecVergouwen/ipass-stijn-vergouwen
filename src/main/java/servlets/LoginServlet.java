package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Model.Lid;
import Model.ServiceProvider;
import Model.TrainingService;

public class LoginServlet extends HttpServlet{
	
	public void init() throws ServletException{
		TrainingService service = ServiceProvider.getTrainingService();
		service.setAanwezigheden();
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		TrainingService service = ServiceProvider.getTrainingService();
		
		String naam = request.getParameter("naam");
		String wachtwoord = request.getParameter("wachtwoord");
		
		Lid l = service.loginUser(naam, wachtwoord);
		if(l != null){
			try {
	            response.addCookie(new Cookie("naam", l.getNaam()));
	            request.getSession().setAttribute("loggedUser", l);
	            request.getSession().setAttribute("trainingvanvandaag", service.getTrainingVanVandaag());
	            request.getSession().setAttribute("openaars", service.getAlleOpenaars());
	            request.getRequestDispatcher("/trainingsite/home.jsp").forward(request, response);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
			return;
		}
		request.setAttribute("loginAlert", "Naam en wachtwoord komen niet overeen, probeer opnieuw.");
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}
}
