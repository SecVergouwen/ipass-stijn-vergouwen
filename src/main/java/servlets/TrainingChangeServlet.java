package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Model.ServiceProvider;
import Model.Training;
import Model.TrainingService;

public class TrainingChangeServlet extends HttpServlet{
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		TrainingService service = ServiceProvider.getTrainingService();
		
		String openingstijd = request.getParameter("openingstijd");
		String openaar = request.getParameter("openaar");
		Training trainingvv = (Training) request.getSession().getAttribute("trainingvanvandaag");
		
		service.setOpeningstijd(trainingvv, openingstijd);
		service.setOpenaar(trainingvv, openaar);
		
		request.setAttribute("message", "Training gewijzigd!");
		request.getRequestDispatcher("/trainingsite/trainingbeheren.jsp").forward(request, response);
		
	}
}