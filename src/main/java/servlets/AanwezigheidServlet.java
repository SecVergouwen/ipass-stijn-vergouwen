package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Model.*;

public class AanwezigheidServlet extends HttpServlet{
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		TrainingService service = ServiceProvider.getTrainingService();
		Lid loggedUser = (Lid) request.getSession().getAttribute("loggedUser");
		Training trainingvv = service.getTrainingVanVandaag();
		
		if (request.getParameter("aanwezig") != null){
			loggedUser.setAanwezig(trainingvv);
			request.setAttribute("message", "Je bent aanwezig gemeld bij de training van vandaag!");
			request.getRequestDispatcher("/trainingsite/home.jsp").forward(request, response);
		} else {
			loggedUser.removeAanwezigheid(trainingvv);
			request.setAttribute("message", "Je bent afwezig gemeld bij de training van vandaag!");
			request.getRequestDispatcher("/trainingsite/home.jsp").forward(request, response);
		}
	}
}
