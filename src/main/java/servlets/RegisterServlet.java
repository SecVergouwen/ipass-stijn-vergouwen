package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Model.Lid;
import Model.ServiceProvider;
import Model.TrainingService;

public class RegisterServlet extends HttpServlet{
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		TrainingService service = ServiceProvider.getTrainingService();
		
		String naam = request.getParameter("naam");
		String wachtwoord1 = request.getParameter("wachtwoord1");
		String wachtwoord2 = request.getParameter("wachtwoord2");
		String email = request.getParameter("email");
		
		if (naam.isEmpty() || 
			wachtwoord1.isEmpty() || 
			wachtwoord2.isEmpty() ||
			email.isEmpty()){
			request.setAttribute("registerAlert", "Alle velden moeten gevuld zijn, probeer opnieuw.");
		}else if (!wachtwoord1.equals(wachtwoord2)){
			request.setAttribute("registerAlert", "Wachtwoorden komen niet overeen, probeer opnieuw.");
		}else if(service.registerUser(naam, wachtwoord1, email)){
			request.setAttribute("loginAlert", "Je bent succesvol geregistreerd! Je kunt nu inloggen met je naam en wachtwoord.");
			request.getRequestDispatcher("/index.jsp").forward(request, response);
			return;
		}else{
			request.setAttribute("registerAlert", "Er is iets fout gegaan, probeer opnieuw.");
		}
		
		
		
	}
	
}
