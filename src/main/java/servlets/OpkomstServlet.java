package servlets;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Model.*;

public class OpkomstServlet extends HttpServlet{
	
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		TrainingService service = ServiceProvider.getTrainingService();

		ArrayList<Training> trainingenvorigeweek = service.getTrainingenVorigeWeek();
		ArrayList<Integer> aantalaanwezig = new ArrayList<Integer>();
		
		for (Training t : trainingenvorigeweek) {
			aantalaanwezig.add(service.getAantalAanwezig(t));
		}
		
		request.setAttribute("trainingenvorigeweek", trainingenvorigeweek);
		request.setAttribute("aantalaanwezig", aantalaanwezig);
		request.setAttribute("totaalleden", service.getAlleLeden().size());
		
		request.getRequestDispatcher("/trainingsite/opkomstbekijken.jsp").forward(request, response);
		
	}
}