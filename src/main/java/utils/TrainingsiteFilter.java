package utils;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TrainingsiteFilter implements Filter{
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}
	
	@Override
    public void destroy() {
    }

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletResponse res = ((HttpServletResponse) response);
		HttpServletRequest req = ((HttpServletRequest) request);
		if (req.getSession().getAttribute("loggedUser") == null){
			req.setAttribute("loginAlert", "Please login first!");
			req.getRequestDispatcher("/index.jsp").forward(req, res);
			return;
    	}
		chain.doFilter(request, response);
	}
}