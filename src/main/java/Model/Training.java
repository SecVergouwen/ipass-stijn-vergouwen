package Model;

import java.util.Date;

public class Training {
	private int trainingID;
	private Date datum;
	private String openingstijd;
	private Lid openaar;
	
	public Training(int id, Date dat, String ot, Lid opr){
		trainingID = id;
		datum = dat;
		openingstijd = ot;
		openaar = opr;
	}

	public int getTrainingID() {
		return trainingID;
	}

	public Date getDatum() {
		return datum;
	}

	public String getOpeningstijd() {
		return openingstijd;
	}
	
	public void setOpeningstijd(String ot) {
		openingstijd = ot;
	}

	public Lid getOpenaar() {
		return openaar;
	}
	
	public void setOpenaar(Lid o){
		openaar = o;
	}
	
}
