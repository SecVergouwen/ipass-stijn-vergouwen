package Model;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import Persistence.IsAanwezigDAO;
import Persistence.LidDAO;
import Persistence.TrainingDAO;

public class TrainingService {
	private LidDAO LDAO = new LidDAO();
	private TrainingDAO TDAO = new TrainingDAO();
	private IsAanwezigDAO ADAO = new IsAanwezigDAO();
	private ArrayList<Aanwezig> alleAanwezigheden = ADAO.findAll();
	private ArrayList<Lid> alleLeden = LDAO.findAll();
	private ArrayList<Training> alleTrainingen = TDAO.findAll();
	
	public Lid loginUser(String uNm,String pw){
		if (alleLeden != null){
			for (Lid l : alleLeden) {
				if (l.getNaam().equals(uNm) && l.checkPassword(pw)){
					return l;
				}
			}
		}
		return null;
	}
	
	public boolean registerUser(String nm, String ww, String em){
		if (alleLeden != null){	
			for (Lid l : alleLeden) {
				if (l.getNaam().equals(nm)){
					return false;
				}
			}
		}
		LDAO.addLid((alleLeden.size() + 1), nm, ww, em);
		alleLeden.add(new Lid((alleLeden.size() + 1), nm, ww, em, 0));
		return true;
	}
	
	public ArrayList<Lid> getAlleLeden(){
		return alleLeden;
	}
	
	public Training getTrainingVanVandaag(){
		if (!TDAO.findTrainingVanVandaag().isEmpty()){
			return TDAO.findTrainingVanVandaag().get(0);
		}
		return null;
	}
	
	public void setAanwezigheden(){
		for (Aanwezig a : alleAanwezigheden){
			for (Lid l : alleLeden) {
				if (a.getAccount().getAccountID() == l.getAccountID()){
					l.addAanwezigheid(a.getTraining());
				}
			}
		}
	}
	
	public void addAanwezigheid(Aanwezig a){
		alleAanwezigheden.add(a);
	}
	
	public void removeAanwezigheid(int tID, int aID){
		for (int i = 0; i < alleAanwezigheden.size(); i++){
			if (alleAanwezigheden.get(i).getAccount().getAccountID() == aID &&
				alleAanwezigheden.get(i).getTraining().getTrainingID() == tID){
				alleAanwezigheden.remove(i);
				break;
			}
		}
	}
	
	public ArrayList<Aanwezig> getAlleAanwezigheden(){
		return alleAanwezigheden;
	}
	
	public ArrayList<Lid> getAlleOpenaars(){
		ArrayList<Lid> results = new ArrayList<Lid>();
		
		for (Lid l : alleLeden){
			if (l.getOpeningsRecht() == 1){
				results.add(l);
			}
		}
		return results;
	}
	
	public void setOpeningstijd(Training t, String ot){
		for (Training g : alleTrainingen){
			if (t.getTrainingID() == g.getTrainingID()){
				TDAO.setOpeningstijd(t, ot);
				t.setOpeningstijd(ot);
				g.setOpeningstijd(ot);
			}
		}
	}
	
	public void setOpenaar(Training t, String o){
		for (Lid l : alleLeden){
			if (l.getNaam().equals(o)){
				Lid openaar = l;
				TDAO.setOpenaar(t.getTrainingID(), l.getAccountID());
				t.setOpenaar(openaar);
			}
		}
	}
	
	public ArrayList<Training> getTrainingenVorigeWeek(){
		ArrayList<Training> results = new ArrayList<Training>();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		int dagen = 7;
		Long tijd = date.getTime() - dagen*24*60*60*1000;
		String vorigeweek = sdf.format(new Date(tijd));

		for (Training t : alleTrainingen){
			if (t.getDatum().toString().equals(vorigeweek) && dagen >= 0){
				results.add(t);
				dagen = dagen - 1;
				tijd = date.getTime() - dagen*24*60*60*1000;
				vorigeweek = sdf.format(new Date(tijd));
			}
		}
		
		return results;
	}
	
	public int getAantalAanwezig(Training t){
		ArrayList<Aanwezig> results = new ArrayList<Aanwezig>();
		
		for (Aanwezig a : alleAanwezigheden) {
			if (a.getTraining().getTrainingID() == t.getTrainingID()){
				results.add(a);
			}
		}
		return results.size();
	}
	
}
