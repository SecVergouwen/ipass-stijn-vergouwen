package Model;

public class Aanwezig {
	private Lid account;
	private Training training;
	
	public Aanwezig(Lid a, Training t){
		account = a;
		training = t;
	}
	
	public Training getTraining(){
		return training;
	}

	public Lid getAccount(){
		return account;
	}
	
}
