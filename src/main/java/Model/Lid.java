package Model;

import java.util.ArrayList;
import Persistence.IsAanwezigDAO;

public class Lid {
	private int accountID;
	private String naam;
	private String wachtwoord;
	private String email;
	private int openingsRecht;
	private ArrayList<Aanwezig> aanwezigheden = new ArrayList<Aanwezig>();
	
	public Lid(int id, String nm, String ww, String em, int or){
		accountID = id;
		naam = nm;
		wachtwoord = ww;
		email = em;
		openingsRecht = or;
	}

	public int getAccountID() {
		return accountID;
	}

	public String getNaam() {
		return naam;
	}

	public String getEmail() {
		return email;
	}

	public int getOpeningsRecht() {
		return openingsRecht;
	}
	
	public String getWachtwoord(){
		return wachtwoord;
	}
	
	public boolean checkPassword(String password){
		if (password.equals(this.wachtwoord)){
			return true;
		}else{
			return false;
		}
	}
	
	public ArrayList<Aanwezig> getAanwezigheden() {
		return aanwezigheden;
	}
	
	public void addAanwezigheid(Training t){
		Aanwezig aw = new Aanwezig(this, t);
		boolean alaanwezig = false;
		for (Aanwezig a : aanwezigheden){
			if (a.getTraining().getTrainingID() != t.getTrainingID()){
				alaanwezig = true;
			}
		} if (!alaanwezig){
			aanwezigheden.add(aw);
		}
	}

	public void setAanwezig(Training t){
		IsAanwezigDAO ADAO = new IsAanwezigDAO();
		TrainingService service = ServiceProvider.getTrainingService();
		Aanwezig aw = new Aanwezig(this, t);
		boolean alaanwezig = false;
		
		for (Aanwezig a : service.getAlleAanwezigheden()){
			if (a.getAccount().getAccountID() == accountID && a.getTraining().getTrainingID() == t.getTrainingID()){
				alaanwezig = true;
			}
		}
		
		if (!alaanwezig){
			aanwezigheden.add(aw);
			ADAO.setAanwezig(t.getTrainingID(), accountID);
			service.addAanwezigheid(aw);
		}
	}
	
	public void removeAanwezigheid(Training t){
		IsAanwezigDAO ADAO = new IsAanwezigDAO();
		TrainingService service = ServiceProvider.getTrainingService();
		ADAO.removeAanwezigheid(t.getTrainingID(), accountID);
		service.removeAanwezigheid(t.getTrainingID(), accountID);
		for (Aanwezig a : aanwezigheden){
			if (a.getTraining().getTrainingID() == t.getTrainingID() && a.getAccount().getAccountID() == accountID){
				aanwezigheden.remove(a);
				break;
			}
		}
	}
	
}
