package Persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import Model.Aanwezig;

public class IsAanwezigDAO extends BaseDAO{
	
	private ArrayList<Aanwezig> selectAanwezigheden(String query) {
		ArrayList<Aanwezig> results = new ArrayList<Aanwezig>();
		LidDAO LDAO = new LidDAO();
		TrainingDAO TDAO = new TrainingDAO();
		
		try (Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);
			while (dbResultSet.next()) {
				int accountID = dbResultSet.getInt("accountID");
				int trainingID = dbResultSet.getInt("trainingID");
				Aanwezig nieuweAanwezigheid = new Aanwezig(LDAO.findAll().get(accountID - 1), TDAO.findAll().get(trainingID - 1));
				results.add(nieuweAanwezigheid);
			}
		} catch (SQLException sqle) { sqle.printStackTrace(); }
		
		return results;
	}
	
	public ArrayList<Aanwezig> findAll(){
		return selectAanwezigheden("SELECT * FROM isaanwezig;");
	}
	
	public void setAanwezig(int trainingID, int accountID){
		
		String query = "INSERT INTO isaanwezig VALUES(" + accountID + ", " + trainingID + ");";
	
		try (Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			stmt.executeUpdate(query);
		}
		catch (SQLException sqle) { sqle.printStackTrace(); }
	}
	
	public void removeAanwezigheid(int trainingID, int accountID){
		String query = "DELETE FROM isaanwezig WHERE accountID = " + accountID + " and trainingID = " + trainingID + ";";
		
		try (Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			stmt.executeUpdate(query);
		}
		catch (SQLException sqle) { sqle.printStackTrace(); }
	}
	
}
