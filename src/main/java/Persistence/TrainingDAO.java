package Persistence;

import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import Model.Training;

public class TrainingDAO extends BaseDAO{
	
	private ArrayList<Training> selectTrainingen(String query) {
		ArrayList<Training> results = new ArrayList<Training>();
		LidDAO LDAO = new LidDAO();
		
		try (Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);
			while (dbResultSet.next()) {
				int trainingID = dbResultSet.getInt("trainingID");
				Date datum = dbResultSet.getDate("datum");
				String openingstijd = dbResultSet.getString("openingstijd");
				int openaar = dbResultSet.getInt("openaar");
				Training nieuwTraining = new Training(trainingID, datum, openingstijd, LDAO.findAll().get(openaar - 1));
				results.add(nieuwTraining);
			}
		} catch (SQLException sqle) { sqle.printStackTrace(); }
		
		return results;
	}
	
	public ArrayList<Training> findTrainingVanVandaag(){
		java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
		return selectTrainingen("SELECT * FROM training WHERE datum = '" + date.toString() + "';");
	}
	
	public ArrayList<Training> findAll() {
		return selectTrainingen("SELECT * FROM training");
	}
	
	public void setOpeningstijd(Training t, String o){
		try (Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			String query = "UPDATE `bfunksite`.`training` SET `openingstijd`='" + o + "' WHERE `trainingID`='" + t.getTrainingID() + "';";
			stmt.executeUpdate(query);
		}
		catch (SQLException sqle) { sqle.printStackTrace(); }
	}
	
	public void setOpenaar(int t, int o){
		try (Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			String query = "UPDATE `bfunksite`.`training` SET `openaar`=" + o + " WHERE `trainingID`=" + t + ";";
			stmt.executeUpdate(query);
		}
		catch (SQLException sqle) { sqle.printStackTrace(); }
	}
	
}
