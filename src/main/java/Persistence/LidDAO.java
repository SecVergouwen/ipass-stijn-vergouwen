package Persistence;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import Model.Lid;

public class LidDAO extends BaseDAO{
	
	private ArrayList<Lid> selectLeden(String query) {
		ArrayList<Lid> results = new ArrayList<Lid>();
		
		try (Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			ResultSet dbResultSet = stmt.executeQuery(query);
			while (dbResultSet.next()) {
				int accountID = dbResultSet.getInt("accountID");
				String naam = dbResultSet.getString("naam");
				String wachtwoord = dbResultSet.getString("wachtwoord");
				String email = dbResultSet.getString("email");
				int openingsRecht = dbResultSet.getInt("openingsRecht");
				Lid nieuwLid = new Lid(accountID, naam, wachtwoord, email, openingsRecht);
				results.add(nieuwLid);
			}
		} catch (SQLException sqle) { sqle.printStackTrace(); }
		
		return results;
	}
	
	public ArrayList<Lid> findAll() {
		return selectLeden("SELECT * FROM lid");
	}
	
	public void addLid(int id, String nm, String ww, String em){
		try (Connection con = super.getConnection()){
			Statement stmt = con.createStatement();
			String query = "INSERT INTO lid VALUES("
						   + id + ", '" + nm + "', '"  + ww + "', '"  + em + "', "  + 0 + ");";
			stmt.executeUpdate(query);
		}
		catch (SQLException sqle) { sqle.printStackTrace(); }
	}
	
}
