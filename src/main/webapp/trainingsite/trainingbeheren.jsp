<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="Model.*" %>
<%@ page import="java.util.ArrayList" %>
<% Lid loggedUser = (Lid) request.getSession().getAttribute("loggedUser"); %>
<% Training trainingvv = (Training) request.getSession().getAttribute("trainingvanvandaag"); %>
<% ArrayList<Lid> openaars = (ArrayList<Lid>) request.getSession().getAttribute("openaars"); %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>B-funk Training beheren</title>
		<link rel="stylesheet" type="text/css" href="/style.css">
	</head>
	
	<body>
	
		<div class="display">
			<div class="header">
				<a href="/trainingsite/home.jsp">
					<img src="/B-funk Logo.png" style="width: 15%; padding: 0px 8px;">
				</a>
				<div class="loggedin">
					<b>Welkom,<br>
					${loggedUser.naam}</b><br>
					<a class="button logoutbutton" href="/trainingsite/LogoutServlet.do">Uitloggen</a>
				</div>
			</div>
			
			<div class='nav'>
				<li><a href='/trainingsite/home.jsp'>Home</a></li>
				<li><a href='/trainingsite/OpkomstServlet.do'>Opkomst bekijken</a></li>
				<% if (loggedUser.getOpeningsRecht() == 1){ %>
				<li><a href='/trainingsite/trainingbeheren.jsp'><b>Training beheren</b></a></li>
				<% } %>
		    </div>
		
			<div class="section">
			
				<div class="message" style="padding-top: 8px;">
					<%if (request.getAttribute("message") != null){
					 	out.println(request.getAttribute("message"));
					}%>
				</div>
			
				<% if (trainingvv != null && trainingvv.getOpenaar().getAccountID() == loggedUser.getAccountID()){ %>
				<h1>De training van vandaag, <%= trainingvv.getDatum() %></h1>
				
				<form action="/trainingsite/TrainingChangeServlet.do" method="post">
					<label>Hoelaat wordt de zaal geopend?</label>
					<% if (trainingvv.getOpeningstijd() != null){ %>
					<input type="time" name="openingstijd" value="<%= trainingvv.getOpeningstijd() %>">
					<% } else { %>
					<input type="time" name="openingstijd">
					<% } %>
					<br><br>
					<label>Heb je met iemand afgesproken dat hij/zij de zaal zal openen?<br>
						   Zo ja, kies hier wie:</label>
					<select name="openaar">
						<option value="null">
							geen andere openaar
						</option>
						<c:forEach items="${openaars}" var="openaars">
						    <option value="${openaars.naam}">
						        ${openaars.naam}
						    </option>
						</c:forEach>
					</select>
					<br>
					<input type="submit" value="Wijzig Training">
				</form>
				<% } else {%>
				<h1>De zaal wordt vandaag geopend door <%= trainingvv.getOpenaar().getNaam() %></h1>
				<% } %>
			</div>
		</div>
		
	</body>
</html>