<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="Model.*" %>
<% Lid loggedUser = (Lid) request.getSession().getAttribute("loggedUser"); %>
<% Training trainingvv = (Training) request.getSession().getAttribute("trainingvanvandaag"); %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>B-funk Home</title>
		<link rel="stylesheet" type="text/css" href="/style.css">
	</head>
	
	<body>
	
		<div class="display">
			<div class="header">
				<a href="/trainingsite/home.jsp">
					<img src="/B-funk Logo.png" style="width: 15%; padding: 0px 8px;">
				</a>
				<div class="loggedin">
					<b>Welkom,<br>
					${loggedUser.naam}</b><br>
					<a class="button logoutbutton" href="/trainingsite/LogoutServlet.do">Uitloggen</a>
				</div>
			</div>
			
			<div class='nav'>
				<li><a href='/trainingsite/home.jsp'><b>Home</b></a></li>
				<li><a href='/trainingsite/OpkomstServlet.do'>Opkomst bekijken</a></li>
				<% if (loggedUser.getOpeningsRecht() == 1){ %>
				<li><a href='/trainingsite/trainingbeheren.jsp'>Training beheren</a></li>
				<% } %>
		    </div>
		
			<div class="section">
			
				<div class="message" style="padding-top: 8px;">
					<%if (request.getAttribute("message") != null){
					 	out.println(request.getAttribute("message"));
					}%>
				</div>
			
				<% if (trainingvv != null){ %>
				<h1>De training van vandaag, <%= trainingvv.getDatum() %></h1>
				<table style="text-align: left; border: 1px solid rgba(0,0,0,1); width: 70%; margin: auto;">
					<tr>
						<td>De zaal word vandaag geopend door</td>
						<td><b><%= trainingvv.getOpenaar().getNaam() %></b></td>
					</tr>
					<tr>
						<td>De openingstijd van de zaal is</td>
						<td><b>
						<% if (trainingvv.getOpeningstijd() != null){
								out.println(trainingvv.getOpeningstijd());
							}else{out.println("nog niet opgegeven");}
							%></b></td>
					</tr>
				</table>
				<form action="/trainingsite/AanwezigheidServlet.do" method="post">
					<label>Ben je aanwezig?</label>
					<% boolean aanwezig = false;
						if (loggedUser.getAanwezigheden().size() > 0){
							for (Aanwezig a : loggedUser.getAanwezigheden()){
								if (a.getTraining().getTrainingID() == trainingvv.getTrainingID()){ 
									aanwezig = true;
								 }
							}
					} if (aanwezig){ %>
					<input type="checkbox" name="aanwezig" value="${ trainingvv.trainingID }" checked>
					<% } else { %>
					<input type="checkbox" name="aanwezig" value="${ trainingvv.trainingID }">
					<% } %>
					<input type="submit" value="Opgeven">
					<p>Heb je je al aanwezig gemeld?<br>
					Laat het vinkje leeg en druk op de knop om afwezig te melden</p>
				</form>
				<% } else {%>
				<h1>Er is vandaag geen training.</h1>
				<% } %>
			</div>
		</div>
		
	</body>
</html>