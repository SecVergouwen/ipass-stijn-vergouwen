<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="Model.*" %>
<%@ page import="java.util.*" %>
<% Lid loggedUser = (Lid) request.getSession().getAttribute("loggedUser"); %>
<% int totaalleden = (Integer) request.getAttribute("totaalleden"); %>
<% ArrayList<Training> trainingenvorigeweek = (ArrayList<Training>) request.getAttribute("trainingenvorigeweek"); %>
<% ArrayList<Integer> aantalaanwezig = (ArrayList<Integer>) request.getAttribute("aantalaanwezig"); %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>B-funk Home</title>
		<link rel="stylesheet" type="text/css" href="/style.css">
	</head>
	
	<body>
	
		<div class="display">
			<div class="header">
				<a href="/trainingsite/home.jsp">
					<img src="/B-funk Logo.png" style="width: 15%; padding: 0px 8px;">
				</a>
				<div class="loggedin">
					<b>Welkom,<br>
					${loggedUser.naam}</b><br>
					<a class="button logoutbutton" href="/trainingsite/LogoutServlet.do">Uitloggen</a>
				</div>
			</div>
			
			<div class='nav'>
				<li><a href='/trainingsite/home.jsp'>Home</a></li>
				<li><a href='/trainingsite/OpkomstServlet.do'><b>Opkomst bekijken</b></a></li>
				<% if (loggedUser.getOpeningsRecht() == 1){ %>
				<li><a href='/trainingsite/trainingbeheren.jsp'>Training beheren</a></li>
				<% } %>
		    </div>
		
			<div class="section">
			
				<h1>De opkomst van de laatste week</h1>
				<p>Totaal aantal leden: <%= totaalleden %></p>
				
				<table>
					<tr>
						<th>Datum training</th>
						<th>Leden aanwezig</th>
						<th>Geopend door</th>
						<th>Geopend om</th>
					</tr>
					<c:forEach items="${trainingenvorigeweek}" var="training" varStatus="status">
						<tr>
						    <td>
						        ${training.datum}
							</td>
							<td>
						        ${aantalaanwezig[status.index]}
							</td>
							<td>
						        ${training.openaar.naam}
							</td>
							<td>
						        ${training.openingstijd}
							</td>
						</tr>
					</c:forEach>
				</table>
				
			</div>
		</div>
		
	</body>
</html>