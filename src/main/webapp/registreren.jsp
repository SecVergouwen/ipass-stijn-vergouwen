<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Register</title>
		<link rel="stylesheet" type="text/css" href="/style.css">
	</head>
	
	<body>
	
		<div class="display">
			<div class="header">
				<img src="/B-funk Logo.png" style="width: 15%; padding: 0px 8px;">
			</div>
		
			<div class="section">
				
				<h1>Vul je gegevens in:</h1>
				
				<div class="alert" style="padding: 4px 19px;">
						<% 
							if (request.getAttribute("registerAlert") != null){
								out.println(request.getAttribute("registerAlert"));
							}
						%>
				</div>
				
				<form action="/RegisterServlet.do" method="post" class="loginform">
					<label style="float: left;"><b>Naam (voor en achternaam):</b></label>
					<input type="text" name="naam"><br>
					
					<label style="float: left;"><b>Wachtwoord:</b></label>
					<input type="password" name="wachtwoord1"><br>
					
					<label style="float: left;"><b>Herhaal wachtwoord:</b></label>
					<input type="password" name="wachtwoord2"><br>
					
					<label style="float :left;"><b>Email:</b></label>
					<input type="text" name="email"><br>
					
					<div style="text-align: center;">
						<input type="submit" value="Registreren">
					</div>
				</form>
			</div>
		</div>
		
	</body>
</html>