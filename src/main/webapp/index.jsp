<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Login</title>
		<link rel="stylesheet" type="text/css" href="/style.css">
	</head>
	
	<body>
	
		<div class="display">
			<div class="header">
				<img src="/B-funk Logo.png" style="width: 15%; padding: 0px 8px;">
			</div>
		
			<div class="section">
			
				<div class="alert" style="padding: 4px 19px;">
						<% 
							if (request.getAttribute("loginAlert") != null){
								out.println(request.getAttribute("loginAlert"));
							}
						%>
				</div>
				
				<form action="/LoginServlet.do" method="post" class="loginform">
					<%
		                String naam = "";
						if (request.getCookies() != null){
			                for (Cookie c : request.getCookies()){
			                    if (c != null && c.getName().equals("naam")) naam = c.getValue();
			                }
						}
					%>
					<label style="float: left;"><b>Naam:</b></label>
					<input type="text" name="naam" value="<%= naam %>"><br>
					<label><b>Wachtwoord:</b></label>
					<input type="password" name="wachtwoord"><br>
					<div style="text-align: center;">
						<a class="button logoutbutton" href="/registreren.jsp" style="margin-right: 0;">Registreren</a>
						<input type="submit" value="Inloggen">
					</div>
				</form>
			</div>
		</div>
		
	</body>
</html>