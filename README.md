Welkom op mijn IPASS repository, hier staat alle source code en documentatie van mijn applicatie.

Om mijn documentatie te vinden kunt u de map 'documentatie' openen.

http://trainingbfunkipass-secv.rhcloud.com/index.jsp is de link naar mijn applicatie.
Om de applicatie te kunnen gebruiken kunt u zichzelf registreren, op deze manier krijg je het overzicht van een B-funk lid zonder recht om de training te openen.

Om de applicatie te gebruiken met recht om de training te openen kunt u een van de volgende inlog gegevens gebruiken:
naam: Robin Warmelink
wachtwoord: zomer

naam: Wassim Jouhri
wachtwoord: herfst

naam: Denzel van Russel
wachtwoord: winter

naam: Yoeri Pham
wachtwoord: lente

U kunt op de site de volgende dingen doen:
aanwezig melden voor de training van vandaag,
opkomst per training bekijken van de laatste week,
de openingstijd van de training van vandaag aanpassen,
de openaar van de training van vandaag aanpassen.

Let op!: de functie om de openingstijd of openaar van de training aan te passen WERKT NIET OP OPENSHIFT(deze waardes worden om een of andere reden gereset op openshift), wel op een local server van Tomcat Apache. Waarom het op openshift niet werkt kan ik niet achter komen en aangezien ik aan het einde alles op openshift gezet heb kan ik dit niet meer maken.

Dus niet alles werkt op openshift, maar alle functies van de site werken wel locally.
Als U het op uw computer locally wilt runnen staan er kopieën van de database in het mapje Database.